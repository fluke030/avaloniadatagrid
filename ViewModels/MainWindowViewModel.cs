﻿using DataGrid.Models;
using ReactiveUI;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace DataGrid.ViewModels
{
    public class MainWindowViewModel : ViewModelBase
    {
        private ObservableCollection<Person> _people;

        public ObservableCollection<Person> People 
        {
            get => _people;
            set { this.RaiseAndSetIfChanged(ref _people, value); }
        }

        public MainWindowViewModel()
        {
            var people = new List<Person>
            {
                new Person("Neil", "Armstrong", false),
                new Person("Buzz", "Lightyear", true),
                new Person("James", "Kirk", true)
            };
            People = new ObservableCollection<Person>(people);
        }
    }
}
